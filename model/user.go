package model

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID            primitive.ObjectID `bson:"_id,omitempty"`
	Name          string             `bson:"name" json:"name"`
	Email         string             `bson:"email" json:"email"`
	Password      string             `bson:"password" json:"password"`
	DiaryPassword string             `bson:"diary_password" json:"diary_password"`
	Age           int                `bson:"age"    json:"age"`
	Gender        string             `bson:"gender" json:"gender"`
	Tasks         []Task             `bson:"tasks"  json:"tasks"`
	Timelines     []Timeline         `bson:"timeline" json:"timelines"`
	Contacts      []Contact          `bson:"contacts" json:"contacts"`
	Meetups       []Meetup           `bson:"meetups" json:"meetups"`
	Notes         []Note             `bson:"notes" json:"notes"`
}

//TODO:add is valid user
//TODO:genrate unique from name


func (db *DB) GetAllUser() []User {
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	cursor, err := db.Collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	var objects []User
	if err = cursor.All(ctx, &objects); err != nil {
		log.Fatal(err)
	}
	defer cancel()
	return objects
}

func (db *DB) UserIsalreadyRegistered(email string) bool {
	res := false
	if len(db.GetAllUser()) < 1 {
		res = false
	} else {
		for _, val := range db.GetAllUser() {
			if val.Email == email {
				res = true
				break
			}
		}
	}
	return res
}
