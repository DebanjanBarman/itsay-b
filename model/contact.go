package model

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Contact struct {
	ID    primitive.ObjectID `bson:"_id,omitempty"`
	Name  string             `bson:"name"`
	Email string             `bson:"email"`
	Phone string             `bson:"phone"`
}

func (db *DB) GetAllContact() []Contact {
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	cursor, err := db.Collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	var objects []Contact
	if err = cursor.All(ctx, &objects); err != nil {
		log.Fatal(err)
	}

	defer cancel()
	return objects
}
