package model_test

import (
	"testing"

	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"github.com/stretchr/testify/assert"
)

func TestInstantiate(t *testing.T){
    var db = &model.DB{}
    db = model.Instantiate()
	db.Collection =nil
    assert.Nil(t,db.Collection,"its working")
}