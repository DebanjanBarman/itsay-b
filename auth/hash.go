package auth

import (
	// "bytes"
	"fmt"

	"golang.org/x/crypto/bcrypt"
)
	 

func GenrateHash(pas string)string{
	pass := []byte(pas)
	pass,err:=bcrypt.GenerateFromPassword(pass,bcrypt.DefaultCost)
	if err!=nil{
		fmt.Printf("err in hashing :\t%v\n",err)
		return ""
	}
	return string(pass)
}

func CompareHashes(pass []byte, pass1 string)bool{
	pass2 := []byte(pass1)
	err:=bcrypt.CompareHashAndPassword(pass,pass2)
	if err !=nil{
		fmt.Printf("err in comparing :\t%v\n",err)
		return false
	}
	return true
}