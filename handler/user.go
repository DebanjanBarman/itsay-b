package handler

import (
	"encoding/json"
	"fmt"

	"net/http"

	"github.com/rishi-org-stack/ItsaboutYou-b/auth"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"github.com/rishi-org-stack/ItsaboutYou-b/response"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "golang.org/x/tools/go/analysis/passes/nilfunc"
)

var db = model.Instantiate().Connect()
var	res = &response.Response{}
//TODO:Login authentican pai work kro
type Query struct {
	Where interface{}
	Val   interface{}
}

const (
	database = "ITSABOUTYOU"
)

func Shout(w http.ResponseWriter, r *http.Request) {
	val := make(map[string]string)
	val["weather"] = "is great"
	json.NewEncoder(w).Encode(val)
}

func Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type","application/json")
	var p model.User
	// res := &response.Response{}
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		res.ServerError(err.Error(),r.Body)
	}
	db.CreateDb(database)
	db.LinkToCollection("user")

	if db.UserIsalreadyRegistered(p.Email){
		res.AlreadyPresent("User is already registerd",nil)

	}else{

		p.Password = (auth.GenrateHash(p.Password))
		p.DiaryPassword = auth.GenrateHash(p.DiaryPassword)
		id:=db.Insert(p)
		p.ID=id.(primitive.ObjectID)
		res.Success("User is Successfully registered",p)
	}
	json.NewEncoder(w).Encode(res)
}

func GetaUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "json")
	var q Query
	var p model.User
	// var res = &response.Response{}
	json.NewDecoder(r.Body).Decode(&q)
	db.CreateDb(database)
	db.LinkToCollection("user")
	err:=db.Get(q.Where.(string), q.Val, &p)
	if err!=nil{
		res.NosuchDoc(err.Error(),nil)
	}
	res.Success("success",p)
	json.NewEncoder(w).Encode(res)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "json")
	var q Query
	json.NewDecoder(r.Body).Decode(&q)
	db.CreateDb(database)
	db.LinkToCollection("user")
	c,err:=db.UpdateaDocument(q.Where.(map[string]interface{}), q.Val.(map[string]interface{}))
	if err !=nil{
		res.ErrorUpdateDoc(err.Error(),c)
	}
	res.Success("Update succesfull",q)
	json.NewEncoder(w).Encode(res)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "json")
	var q Query
	json.NewDecoder(r.Body).Decode(&q)
	db.CreateDb(database)
	fmt.Println(q)
	db.LinkToCollection("data")

	count ,err:= db.DeleteADocument(q.Where.(map[string]interface{}))
	if err!=nil{
		res.ErrorDeleteDoc(err.Error(),count)
	}
	res.Success("Delete succesfull",q)
	json.NewEncoder(w).Encode(res)
}