package server

import (
	// "encoding/json"
	// "net/http"
	hnd "github.com/rishi-org-stack/ItsaboutYou-b/handler"

	"github.com/gorilla/mux"
)

func Route() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/", hnd.Shout).Methods("GET","POST")
	userRoute:=r.PathPrefix("/user").Subrouter()
	Taskroute:=r.PathPrefix("/task").Subrouter()
	contactRoute:=r.PathPrefix("/contact").Subrouter()
	meetupRoute:=r.PathPrefix("/meetup").Subrouter()
	timelineRoute:=r.PathPrefix("/timeline").Subrouter()
	admin:=r.PathPrefix("/admin").Subrouter()
	admin.HandleFunc("/",hnd.Shout)
	userRoute.HandleFunc("/",hnd.Register).Methods("POST")//this will  be ued for registration
	userRoute.HandleFunc("/login",hnd.Shout)//login route for user
	userRoute.HandleFunc("/get",hnd.GetaUser).Methods("GET")//this will be used for user profile page
	userRoute.HandleFunc("/update",hnd.UpdateUser).Methods("PUT")
	userRoute.HandleFunc("/delete",hnd.DeleteUser).Methods("DELETE")
	Taskroute.HandleFunc("/",hnd.Shout)
	contactRoute.HandleFunc("/",hnd.Shout)
	meetupRoute.HandleFunc("/",hnd.Shout)
	timelineRoute.HandleFunc("/",hnd.Shout)
	return r
}
