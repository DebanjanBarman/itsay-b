module github.com/rishi-org-stack/ItsaboutYou-b

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/rs/cors v1.7.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	go.mongodb.org/mongo-driver v1.5.0 // indirect
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	golang.org/x/tools v0.0.0-20190531172133-b3315ee88b7d // indirect
)
